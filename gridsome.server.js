/**
 * Ubuntu Touch devices website
 * Copyright (C) 2021 UBports Foundation <info@ubports.com>
 * Copyright (C) 2021 Jan Sprinz <neo@neothethird.de>
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const requireMaybe = require("require-maybe");

const portStatus = require("./data/portStatus.json");
const deviceInfo = require("./data/deviceInfo.json");
const progressStages = require("./data/progressStages.json");
const installerData = requireMaybe("./data/installerData.json") || [];

if (!installerData.length) {
  console.log(
    "\x1b[33m%s\x1b[0m",
    "Installer data unavailable at build time.\n",
    "You should consider running one of the following commands instead:\n",
    "npm run develop : Fetch data and run in development mode\n",
    "npm run build : Fetch data and build site for production mode\n",
    "npm run get-data : Only fetch data"
  );
}

module.exports = function(api) {
  // Load port status and device info collections
  api.loadSource(store => {
    const portInfo = store.addCollection("portInfo");

    portInfo.addNode({
      id: "portStatusData",
      data: portStatus
    });

    portInfo.addNode({
      id: "deviceInfoData",
      data: deviceInfo
    });
  });

  api.onCreateNode(options => {
    if (options.internal.typeName === "Device") {
      // Set codename from file name
      options.codename = options.fileInfo.name;

      // Calculate porting progress from feature matrix ( use maturity field as fallback )
      if (options.portStatus) {
        let totalWeight = 0,
          currentWeight = 0,
          currentStageIndex = progressStages.length - 1; // Daily-driver ready

        for (let category of options.portStatus) {
          for (let feature of category.features) {
            let featureData = portStatus.find(
              el => el.id == feature.id && el.category == category.categoryName
            );

            // Calculate progress (from weights)
            totalWeight += featureData.weight;
            currentWeight += feature.value == "+" ? featureData.weight : 0;

            // Calculate progress stage
            if (feature.value == "-") {
              let featureStageIndex = progressStages.indexOf(featureData.stage);
              if (
                currentStageIndex >= featureStageIndex &&
                featureStageIndex > 0
              ) {
                currentStageIndex = featureStageIndex - 1;
              }
            }
          }
        }

        options.progress =
          Math.round((1000 * currentWeight) / totalWeight) / 10;
        options.progressStage = progressStages[currentStageIndex];

        // Fallback from maturity
      } else {
        options.progress = options.maturity * 100;
        options.progressStage = progressStages[0];
        console.log(
          "\x1b[33m%s\x1b[0m",
          "Using maturity as fallback for: " + options.name
        );
      }

      // Import installer compatibility data
      let deviceCodenameAlias = options.installerAlias
        ? options.installerAlias
        : options.codename;
      let deviceInstaller = installerData.some(
        el =>
          el.codename == deviceCodenameAlias &&
          el.operating_systems.includes("Ubuntu Touch")
      );
      options.noInstall = !deviceInstaller;
    }
  });
};
